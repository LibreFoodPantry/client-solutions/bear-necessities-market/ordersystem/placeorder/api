const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const PORT = 4000;
const mongoPort = "mongo";
const path = require('path');
//const HOMEPORT = 3000;

app.use(cors());
app.use(bodyParser.json());

//Default Route
app.get('/api', (req, res) => {
    res.status(200).send('Place Order API');
});

/*
//homepage route
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + 'localhost:3000'));
});
*/

//Import Routes
const orderRoutes = require('./routes/orders');
app.use('/api/orders', orderRoutes);

//Connection to the Database
mongoose.connect('mongodb://'.concat(mongoPort).concat(':27017/Orders'), { useNewUrlParser: true});
const connection = mongoose.connection;
connection.once('open', function(){
    console.log("MongoDB connection established");
});

app.listen(PORT, function(){
    console.log("Server is running on port:" + PORT);
});
