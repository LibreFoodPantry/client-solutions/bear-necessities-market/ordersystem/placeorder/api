FROM node:16.1.0-alpine

RUN mkdir -p /srv/app/Api
WORKDIR /srv/app/Api

COPY package.json /srv/app/Api
COPY package-lock.json /srv/app/Api

RUN npm install --silent

COPY . /srv/app/Api

EXPOSE 4000

# CMD ["mongod"]

CMD ["node", "server.js"]
