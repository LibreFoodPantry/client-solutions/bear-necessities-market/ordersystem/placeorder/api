
# PlaceOrder API Documentation

## Table Of Contents
- [1.1: Purpose](#11-purpose)
- [1.2: Development Container Instructions](#12-development-container-instructions)
  - [1.2.1: Prerequisites](#121-prerequisites)
  - [1.2.2: Using the Development Environment](#122-using-the-development-environment)
  - [1.2.3: Troubleshooting Docker](#123-troubleshooting-docker)
- [1.3: Node Package Manager Scripts](#13-node-package-manager-scripts)
  - [1.3.1: Swagger Scripts](#131-swagger-scripts)
  - [1.3.1: TCases Scripts](#132-tcases-scripts)
- [1.4: Install Instructions](#14-install-instructions)
- [1.5: Current State](#15-current-state)
- [1.6: API Developer Documentation](#16-api-developer-documentation)
  - [1.6.1: GET all the orders](#161-get-all-the-orders)
  - [1.6.2: GET a order with the given order ID](#162-get-a-order-with-the-given-order-id)
  - [1.6.3: GET all the orders placed by the given email](#163-get-all-the-orders-placed-by-the-given-email)
  - [1.6.4: GET all the orders placed on the given date and time](#164-get-all-the-orders-placed-on-the-given-date-and-time)
  - [1.6.5: PUT a new order](#165-put-a-new-order)
  - [1.6.6: DELETE a order with the given order ID](#166-delete-a-order-with-the-given-order-id)
  - [1.6.7: To Be Created](#167-to-be-created)

## 1.1: Purpose

This is an OpenAPI specification of the PlaceOrder API. 

The API will allow communication between the Front End, the Back End, and the inventory system. Since, communication will be established a user of BNM will be able to place orders completely online. 

The entry point of the specification is in `src/index.yaml`

---
## 1.2: Development Container Instructions

### 1.2.1: Prerequisites

  Before we can get to using the provided Development Container, we must first:

  - install [Docker](https://docs.docker.com/get-docker/)
  - install [Visual Studio Code](https://code.visualstudio.com)
  - install the Visual Studio Code Extension [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

### 1.2.2: Using the Development Environment

  Once we have Docker, Visual Studio Code, and the Remote - Containers extension all installed, and Docker and Visual Studio Code are both running, we can proceed. There are two ways to start using a development environment:

  - When the project is opened through Visual Studio Code, you should get a notification at the bottom right of your window which shows:
    ![VS Code Remote Containers Notification](https://code.visualstudio.com/assets/docs/remote/containers/dev-container-reopen-prompt.png)
    - From here, you should click ```Reopen in Container```.

or

  - If you did not get a notification, you can click the green button in the bottom left of your window that looks like:
    ![VS Code Remote Containers Icon](https://code.visualstudio.com/assets/docs/remote/containers-tutorial/remote-status-bar.png)
    - You should then be prompted with:
      ![VS Code Remote Containers Menu](https://code.visualstudio.com/assets/docs/remote/containers/remote-containers-reopen.png)
      - From here, you will want to click ```Reopen in Container```.

Visual Studio Code should now start loading the Development Environment.

### 1.2.3: Troubleshooting Docker

1. If you receive an error popup dialogue stating that virtualization needs to be enabled in the BIOS, you'll need to restart your computer, press F2 on the splash screen to go into the BIOs, and then, once inside there, look for the option to toggle hardware virtualization on. Once that's on your computer has to be restarted again.  
![virtualization_error](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/api/uploads/6c99e01944425c512dcde799fe326704/virtualization_error.PNG)

2. If you receive an error popup dialogue in Docker Desktop stating that the WSL2 Linux Kernel needs to be installed, this kernel can be found at [https://aka.ms/wsl2kernel](https://aka.ms/wsl2kernel). Once that is installed, your computer has to be restarted.  
![docker_install](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/api/uploads/aca9eb8e21c770f689a44b7dcd9a374b/docker_install.PNG)

3. If any other issues arise, you may be able find a solution using the official [Docker Troubleshoot Manual](https://docs.docker.com/desktop/windows/troubleshoot/).

---
## 1.3: Node Package Manager Scripts

### 1.3.1: Swagger Scripts

Swagger scripts are being used to verify the api specificication, and ensure the code is free of errors.
- npm run validate
  - validates the ```src/index.yaml``` file to ensure there are no errors.
- npm run bundle ```file-name```
  - bundles the api as the specified output file.

### 1.3.2: TCases Scripts

TCases scripts are being used to generates test case for the api to allow for tests to be run. This will allow us to verify that the api is working as intended.
- npm run tcases
- npm run tcases-anon
- npm run tcases-api ```file-name```
  - generates test cases for the specified bundled file.
- npm run tcases-api-test
- npm run tcases-exec
- npm run tcases-reducer

---
## 1.4: Install Instructions

Currently there are no installation steps. There will be an installation process soon.

---
## 1.5: Current State

The current state of the project is still in it's beginning stage. We have translated all previous JavaScript code to YAML. Also, the project has been containerized with Docker and images with dependencies to build, test, and deploy will be created shortly. The pipeline for CI/CD has been created but is also in it's early stages. More tests and records for said tests will be created soon.   

<br>

**Repo** [Paths Directory](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/api/-/tree/main/src/paths)

**Language:** [YAML](https://circleci.com/blog/what-is-yaml-a-beginner-s-guide/)

**Version:** 1.0

---
## 1.6: API Developer Documentation

### 1.6.1: GET all the orders

**GET** - `http://localhost:4000/api/orders/get`  
**Status**
- `200` - **OK** - All the orders have been returned
- `404` - **Not Found** - There are no orders
- `400` - **BadRequest** - The request has was bad
- `500` - **ServerError** - Error originating from the server

**Return Format**:

### 1.6.2: GET a order with the given order ID

**GET** - `http://locahost:4000/api/orders/get/id/:orderID`  
**Status**
- `200` - **OK** - The specific order has been returned
- `404` - **Not Found** - No order exists with given order ID
- `400` - **BadRequest** - The request has was bad
- `500` - **ServerError** - Error originating from the server

**Return Format**:

### 1.6.3: GET all the orders placed by the given email

**GET** - `http://locahost:4000/api/orders/get/email/:email`  
**Status**
- `200` - **OK** - All the orders placed by given email have been returned
- `404` - **Not Found** - The email hasn't placed any orders
- `400` - **BadRequest** - The request has was bad
- `500` - **ServerError** - Error originating from the server

**Return Format**:

### 1.6.4: GET all the orders placed on the given date and time

**GET** - `http://locahost:4000/api/orders/get/date/:orderDate`  
**Status**
- `200` - **OK** - All the orders placed on given timestamp have been returned
- `404` - **Not Found** - There are no orders placed on that timestamp
- `400` - **BadRequest** - The request has was bad
- `500` - **ServerError** - Error originating from the server

**Return Format**:

### 1.6.5: PUT a new order

**PUT** - `http://locahost:4000/api/orders/post`  
**Status**
- `201` - **Created** - The orders has been created
- `204` - **No Content** - The request has incorrect format
- `400` - **BadRequest** - The request has was bad
- `404` - **NotFound** - The request was not found
- `500` - **ServerError** - Error originating from the server

**Order Components**
- itemsOrdered
  -  **items ordered are listed and seperated by , (comma)**.
- dietaryRestrictions
  - If the order has any dietary restriction or allergies then they are listed here as a **paragraph**.
- commentsPreferences
  - If the order has any specific comments or preferences regarding any item in the order then they are listed here as a **paragraph**.
- email
  - The **String** value of person's email who is placing the order.
- emailVerified
  - A **boolean** value that is a confirmation of the user's email ID match in two different fields. If the system is not checking for it, this can be passed as **true**.
- dateOrdered **(optional)**
  - The **timestamp** the order was placed. If not provided the current system timestamp will be considered.
- orderStatus
  - (PickedUp, Placed, ReadyforPickup) This identifies the current status of an order

**Arguments Format**:

**Return Format**:

**Example Request**

### 1.6.6: DELETE a order with the given order ID

**DELETE** - `https://localhost:4000/api/orders/delete/id/:orderID`  
**Status**
- `204` - **OK** - The order with the given order ID has been deleted
- `404` - **Not Found** - No order exists with given order ID
- `400` - **BadRequest** - The request has was bad
- `404` - **NotFound** - The request was not found
- `500` - **ServerError** - Error originating from the server

**Return Format**: 

### 1.6.7: To Be Created
  - GetbyOrderStatus
    - Show all in progress orders
    - Show all placed orders
    - Show all Complete orders
